package main

import (
	"fmt"
	"net/http"
	"encoding/xml"
	"io/ioutil"
)

type Track struct {
	XMLName xml.Name `xml:"track"`
	Location string `xml:"location"`
	Title string `xml:"title"`
	Creator string `xml:"creator"`
	Image string `xml:"image"`
	Info string `xml:"info"`
}

type TrackList struct {
	XMLName xml.Name `xml:"trackList"`
	Tracks []Track `xml:"track"`
}

type PlayList struct {
	XMLName xml.Name `xml:"playlist"`
	TrackList TrackList `xml:"trackList"`
}

func main() {
	fmt.Printf("Hello, world.\n")
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/sink/", sinkHandler)
	http.ListenAndServe(":8080", nil)
}
func rootHandler (w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "<h2>you dont got to worry about nothing</h2>")
}

func sinkHandler (w http.ResponseWriter, r *http.Request) {
	//http://battleofthebits.org/player/Playlist/battle/2333
	response, err := http.Get("http://battleofthebits.org/player/Playlist/battle/2333")
	if err != nil {
		fmt.Fprintln(w, "%s", err)
		return
	} else {
		defer response.Body.Close()
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Fprintln(w, "%s", err)
			return
		} else {
			//fmt.Fprint(w,string(body))
			t := PlayList{}
			if xml.Unmarshal(body, &t) != nil {
				fmt.Fprintln(w, "%s", err)
				return
			}
			fmt.Fprintln(w, t)
		}
		
	}
}
